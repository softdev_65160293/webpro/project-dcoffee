import { IsNotEmpty, Length } from 'class-validator';
export class CreateUserDto {
  @IsNotEmpty()
  @Length(2, 32)
  name: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  unit: number;

  @IsNotEmpty()
  minimum: number;

  @IsNotEmpty()
  types: 'pack' | 'bottle';
}
