import { Module } from '@nestjs/common';
import { MaterialService } from './material.service';
import { UsersController } from './material.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Material } from './entities/material.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Material])],
  controllers: [UsersController],
  providers: [MaterialService],
})
export class UsersModule {}
