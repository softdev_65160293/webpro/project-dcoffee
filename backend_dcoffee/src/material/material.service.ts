import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Material } from './entities/material.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MaterialService {
  constructor(
    @InjectRepository(Material)
    private userRepository: Repository<Material>,
  ) {}

  create(createUserDto: CreateUserDto): Promise<Material> {
    return this.userRepository.save(createUserDto);
  }

  findAll(): Promise<Material[]> {
    return this.userRepository.find();
  }

  findOne(id: number) {
    return this.userRepository.findOneBy({ id: id });
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return this.userRepository.update(id, updateUserDto);
  }

  async remove(id: number) {
    const deleteUser = await this.userRepository.findOneBy({ id });
    return this.userRepository.remove(deleteUser);
  }
}
