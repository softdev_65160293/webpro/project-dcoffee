import { IsInt, IsNotEmpty, Length } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @Length(1, 32)
  name: string;

  @IsNotEmpty()
  @IsInt()
  price: number;

  @IsNotEmpty()
  @IsInt()
  type: number;
}
