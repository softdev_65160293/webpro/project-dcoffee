import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue')
    },
    {
      path: '/MainMenu',
      name: 'mainmenu',
      component: () => import('../views/MainMenuView.vue')
    },
    {
      path: '/Product',
      name: 'product',
      component: () => import('../views/Product/ProductsView.vue')
    },
    {
      path: '/Employee',
      name: 'employee',
      component: () => import('../views/EmployeeView.vue')
    },
    {
      path: '/Member',
      name: 'member',
      component: () => import('../views/Member/MemberView.vue')
    },
    {
      path: '/POS',
      name: 'pos',
      component: () => import('../views/POS/POSView.vue')
    },
    {
      path: '/Stock',
      name: 'stock',
      component: () => import('../views/Stock/StockView.vue')
    },
    {
      path: '/Bills',
      name: 'bills',
      component: () => import('../views/BillsView.vue')
    },
    {
      path: '/CheckIn',
      name: 'checkin',
      component: () => import('../views/CheckInOutView.vue')
    },
    {
      path: '/Salary',
      name: 'salary',
      component: () => import('../views/SalaryView.vue')
    },
    {
      path: '/Receipt',
      name: 'receipt',
      component: () => import('../views/ReceiptView.vue')
    },
    {
      path: '/CheckStock',
      name: 'checkstock',
      component: () => import('../views/Stock/CheckStockView.vue')
    },
    {
      path: '/OrderStock',
      name: 'orderstock',
      component: () => import('../views/Stock/OrderStockView.vue')
    }
  ]
})

export default router
