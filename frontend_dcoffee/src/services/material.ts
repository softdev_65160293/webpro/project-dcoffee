import type { Material } from '@/types/Material'
import http from './http'

function addMaterial(material: Material) {
  return http.post('/users/', material) //add
}

function updateMaterial(material: Material) {
  return http.patch('/users/' + material.id, material) //update
}

function removeMaterial(material: Material) {
  return http.delete('/users/' + material.id) //delete
}

function getMaterial(id: number) {
  return http.get('/users' + id)
}

function getMaterials() {
  return http.get('/users')
}

export default { addMaterial, updateMaterial, removeMaterial, getMaterial, getMaterials }
