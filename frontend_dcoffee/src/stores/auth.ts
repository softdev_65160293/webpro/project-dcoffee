import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import { fa } from 'vuetify/locale'

export const useAuthStore = defineStore('auth', () => {
  const currentUser = ref<User | null>({
    id: 1,
    email: 'Dcoffee@gmail.com',
    username: 'admin',
    password: '1234',
    fullname: 'Admin Staff',
    gender: 'male',
    roles: ['admin', 'user']
  })

  const defaultUser = ref<User | null>({
    id: 1,
    email: 'Dcoffee@gmail.com',
    username: 'admin',
    password: '1234',
    fullname: 'Admin Staff',
    gender: 'male',
    roles: ['admin', 'user']
  })

  const nonUser = ref<User | null>({
    id: 0,
    email: '',
    username: '',
    password: '',
    fullname: '',
    gender: 'male',
    roles: ['user']
  })

  const users = ref<User[]>([
    {
      id: 1,
      email: 'Dcoffee@gmail.com',
      username: 'admin1',
      password: '1234',
      fullname: 'Admin1 Staff1',
      gender: 'male',
      roles: ['admin']
    },
    {
      id: 2,
      email: 'Dcoffee@gmail.com',
      username: 'admin2',
      password: '1234',
      fullname: 'Admin2 Staff2',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 3,
      email: 'Dcoffee@gmail.com',
      username: 'admin3',
      password: '2345',
      fullname: 'Admin3 Staff3',
      gender: 'male',
      roles: ['user']
    }
  ])

  const status = ref(false)

  const searchUser = (username: string, password: string) => {
    const indexUsername = users.value.findIndex((item) => item.username === username)
    const indexPassword = users.value.findIndex((item) => item.password === password)
    if (indexUsername < 0 && indexPassword < 0) {
      currentUser.value = null
      status.value = false
    } else if (password === users.value[indexUsername].password) {
      currentUser.value = users.value[indexUsername]
      status.value = true
    }
  }

  let onHome = true
  let onMain = false

  function loginSucess() {
    onHome = false
    onMain = true
  }

  const openNvaAdmin = ref(false)
  const openNvaStaff = ref(false)
  const appBarLogin = ref(false)
  const appBarUnlogin = ref(true)

  function checkRole() {
    const roleStatus = ref(0)
    for (const item of currentUser.value!.roles) {
      if (item === 'admin') {
        roleStatus.value = 1
      }
    }

    if (roleStatus.value === 1) {
      console.log(roleStatus.value)
      openNvaAdmin.value = true
      openNvaStaff.value = false
    } else if (roleStatus.value === 0) {
      console.log(roleStatus.value)
      openNvaAdmin.value = false
      openNvaStaff.value = true
    }
  }

  function changeAppBar() {
    appBarLogin.value = true
    appBarUnlogin.value = false
  }

  return {
    currentUser,
    users,
    status,
    onHome,
    onMain,
    openNvaStaff,
    openNvaAdmin,
    changeAppBar,
    loginSucess,
    searchUser,
    checkRole,
    appBarLogin,
    appBarUnlogin,
    defaultUser,
    nonUser
  }
})
