import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Bills } from '@/types/Bills'

export const useBillsStore = defineStore('bills', () => {
  const defalutBills: Bills = {
    id: 0,
    type: 'Electric',
    usedUnit: 0,
    unitToBath: 0,
    totalPrice: 0,
    date: new Date()
  }

  const billsElectric = ref<Bills[]>([
    {
      id: 1,
      type: 'Electric',
      usedUnit: 3,
      unitToBath: 10,
      totalPrice: 30,
      date: new Date(2024, 1, 1, 10, 33, 30)
    },
    {
      id: 2,
      type: 'Electric',
      usedUnit: 4,
      unitToBath: 10,
      totalPrice: 40,
      date: new Date(2024, 1, 2, 11, 44, 30)
    },
    {
      id: 3,
      type: 'Electric',
      usedUnit: 5,
      unitToBath: 10,
      totalPrice: 50,
      date: new Date(2024, 1, 3, 16, 45, 20)
    }
  ])

  const billsWater = ref<Bills[]>([
    {
      id: 1,
      type: 'Water',
      usedUnit: 10,
      unitToBath: 10,
      totalPrice: 100,
      date: new Date(2024, 1, 1, 9, 45, 30)
    },
    {
      id: 2,
      type: 'Water',
      usedUnit: 4,
      unitToBath: 10,
      totalPrice: 40,
      date: new Date(2024, 1, 2, 8, 45, 30)
    },
    {
      id: 3,
      type: 'Water',
      usedUnit: 5,
      unitToBath: 20,
      totalPrice: 100,
      date: new Date(2024, 1, 3, 11, 55, 30)
    }
  ])

  const billsPlace = ref<Bills[]>([
    {
      id: 1,
      type: 'Place',
      usedUnit: 0,
      unitToBath: 0,
      totalPrice: 4000,
      date: new Date(2024, 1, 1, 13, 24, 30)
    },
    {
      id: 2,
      type: 'Place',
      usedUnit: 0,
      unitToBath: 0,
      totalPrice: 5000,
      date: new Date(2024, 1, 2, 14, 22, 30)
    },
    {
      id: 3,
      type: 'Place',
      usedUnit: 0,
      unitToBath: 0,
      totalPrice: 3000,
      date: new Date(2024, 1, 3, 15, 17, 30)
    }
  ])

  return {
    billsWater,
    billsPlace,
    billsElectric,
    defalutBills
  }
})
