import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useEmp = defineStore('Employee', () => {
  const currentUser: User = {
    id: -1,
    email: '',
    username: '',
    password: '',
    fullname: '',
    roles: ['user', 'admin'],
    gender: 'male'
  }
  const emps = ref<User[]>([
    {
      id: 1,
      email: 'Dcoffee@gmail.com',
      username: 'admin',
      password: '1234',
      fullname: 'Admid Staff',
      gender: 'male',
      roles: ['user', 'admin']
    },
    {
      id: 2,
      email: 'Dcoffee@gmail.com',
      username: 'admin',
      password: '1234',
      fullname: 'Admid Staff',
      gender: 'male',
      roles: ['user', 'admin']
    },
    {
      id: 3,
      email: 'Dcoffee@gmail.com',
      username: 'admin',
      password: '1234',
      fullname: 'Admid Staff',
      gender: 'male',
      roles: ['user', 'admin']
    }
  ])
  return {
    emps,
    currentUser
  }
})
