import { ref, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { useReceipetStore } from './receipt'

export const useMembersStore = defineStore('members', () => {
  const receiptStore = useReceipetStore()

  const defaultMembers: Member = {
    id: 0,
    name: '',
    tel: '000000000',
    point: 0
  }

  const members = ref<Member[]>([
    {
      id: 1,
      name: 'มานะ รักชาติ',
      tel: '0881236312',
      point: 150
    },
    {
      id: 2,
      name: 'มานี มีใจ',
      tel: '081134612',
      point: 300
    },
    {
      id: 3,
      name: 'สำรวย รอบฟ้า',
      tel: '0123456789',
      point: 400
    }
  ])

  const currentMember = ref<Member | null>()

  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) currentMember.value = null
    currentMember.value = members.value[index]
    receiptStore.receipt.memberID = currentMember.value.id
  }

  function clear() {
    currentMember.value = null
  }

  return {
    members,
    defaultMembers,
    searchMember,
    clear,
    currentMember
  }
})
