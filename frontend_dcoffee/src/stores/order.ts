import type { Material } from '@/types/Material'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { OrderReceipt } from '@/types/Order'
import { useAuthStore } from './auth'
import type { OrderItems } from '@/types/OrderItems'
import { useStockStore } from './stock'

export const useOrderStore = defineStore('order', () => {
  const authStore = useAuthStore()
  const stockStore = useStockStore()

  const orderItems = ref<OrderItems[]>([])
  const createDate = new Date()
  const orderDialog = ref(false)
  const conDialog = ref(false)
  let lastestId = 0
  let lastestIdReceipt = 0

  const orderReceipt = ref<OrderReceipt>({
    id: 0,
    createdDate: new Date(),
    totalPrice: 0,
    totalQty: 0,
    userId: authStore.currentUser!.id,
    user: authStore.currentUser!
  })

  function addOrderReceipt(material: Material) {
    const index = orderItems.value.findIndex((item) => item.material?.id === material.id)
    if (index >= 0) {
      orderItems.value[index].qty++
    } else {
      const newReceipt: OrderItems = {
        id: lastestId,
        name: material.name,
        price: material.price,
        qty: 1,
        materialId: material.id,
        material: material
      }
      lastestId++
      orderItems.value.push(newReceipt)
    }
    calOrder()
  }

  function calOrder() {
    let totalPrice = 0
    let totalQty = 0

    for (const item of orderItems.value) {
      totalPrice += item.price * item.qty
      totalQty += item.qty
    }

    orderReceipt.value.totalPrice = totalPrice
    orderReceipt.value.totalQty = totalQty
  }

  function openDialog() {
    if (orderItems.value.length === 0) {
      return
    } else {
      orderDialog.value = true
      calOrder()
    }
  }

  function clear() {
    orderItems.value = []
    orderReceipt.value = {
      id: lastestIdReceipt,
      createdDate: new Date(),
      totalPrice: 0,
      totalQty: 0,
      userId: authStore.currentUser!.id,
      user: authStore.currentUser!
    }
  }

  function closeDialog() {
    orderDialog.value = false
  }

  function Confirm() {
    conDialog.value = true
  }

  function Order() {
    for (const items of orderItems.value) {
      const index = stockStore.materials.findIndex((item) => item.name === items.name)
      stockStore.materials[index].unit += items.qty
    }
    lastestIdReceipt++
    closeDialogCon()
    closeDialog()
    clear()
  }

  function closeDialogCon() {
    conDialog.value = false
  }

  function removeReceiptItem(orderItem: OrderItems) {
    const index = orderItems.value.findIndex((item) => item === orderItem)
    if (index !== -1) {
      orderItems.value.splice(index, 1)
    }
    if (orderItems.value.length === 0) {
      closeDialog()
    }
    calOrder()
  }

  return {
    openDialog,
    calOrder,
    addOrderReceipt,
    clear,
    closeDialog,
    Order,
    Confirm,
    closeDialogCon,
    removeReceiptItem,
    createDate,
    orderDialog,
    orderReceipt,
    orderItems,
    conDialog
  }
})
