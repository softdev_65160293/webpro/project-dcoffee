import { ref, nextTick } from 'vue'
import type { VForm } from 'vuetify/components'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/product'

export const useProductsStore = defineStore('products', () => {
  const defaultProducts: Product = {
    id: 0,
    name: '',
    price: 0,
    type: 0
  }

  let LatestId = 0



  const Product2 = ref<Product[]>([
    { id: 11, name: 'Iced Milk Tea', price: 45.0, type: 2 },
    { id: 12, name: 'Iced Tea', price: 40.0, type: 2 },
    { id: 13, name: 'Iced Lemon Tea', price: 45.0, type: 2 },
    { id: 14, name: 'Iced Chocolate', price: 45.0, type: 2 },
    { id: 15, name: 'Iced Latte', price: 45.0, type: 2 },
    { id: 16, name: 'Iced Cappucino', price: 45.0, type: 2 },
    { id: 17, name: 'Iced Espresso', price: 45.0, type: 2 },
    { id: 18, name: 'Iced Thai Milk Tea', price: 45.0, type: 2 },
    { id: 19, name: 'Iced Matcha', price: 45.0, type: 2 }
  ])

  const Product3 = ref<Product[]>([
    { id: 20, name: 'Frappé Milk Tea', price: 55.0, type: 3 },
    { id: 21, name: 'Frappé Chocolate', price: 55.0, type: 3 },
    { id: 22, name: 'Frappé Latte', price: 55.0, type: 3 },
    { id: 23, name: 'Frappé Cappucino', price: 55.0, type: 3 },
    { id: 24, name: 'Frappé Thai Milk Tea', price: 55.0, type: 3 },
    { id: 25, name: 'Frappé Matcha', price: 55.0, type: 3 }
  ])

  const Product4 = ref<Product[]>([
    {
      id: 26,
      name: 'Cake Chocolate',
      price: 55,
      type: 4
    },
    {
      id: 27,
      name: 'Cookie',
      price: 30,
      type: 4
    },
    {
      id: 28,
      name: 'Banana Cake',
      price: 40,
      type: 4
    }
  ])

  function findLatestId(products: Product[]) {
    const latestId = Math.max(...products.map((product: Product) => product.id))
    return latestId
  }

  function LastID() {

    const latestIdP2 = findLatestId(Product2.value)
    const latestIdP3 = findLatestId(Product3.value)
    const latestIdP4 = findLatestId(Product4.value)
    return (LatestId = Math.max(latestIdP2, latestIdP3, latestIdP4))
  }

  const loadingStore = useLoadingStore()
    const products = ref<Product[]>([])
    async function getProduct(id: number) {
        loadingStore.doLoad()
        const res = await productService.getProduct(id)
        products.value = res.data
        loadingStore.finish()
      }
    async function getProducts() {
        loadingStore.doLoad()
        const res = await productService.getProducts()
        products.value = res.data
        loadingStore.finish()
      }
      
      async function saveProduct(product: Product) {
        loadingStore.doLoad()
        if (product.id < 0){
          // Add new
          console.log('Post' + JSON.stringify(product))
          const res = await productService.addProduct(product)
        }else{
          // Update
          console.log('Patch' + JSON.stringify(product))
          const res = await productService.updateProduct(product)
        }
        await getProducts()
        loadingStore.finish()
      }
      async function deleteUser(product: Product){
        loadingStore.doLoad()
        const res = await productService.delProduct(product)
        await getProducts()
        loadingStore.finish()
      }

  // const editedProduct = ref<Product>(JSON.parse(JSON.stringify(initilProduct)))

  // const products = ref<Product[]>([])

  // function onSubmit() {}

  // function closeDelete() {
  //   dialogDelete.value = false
  //   nextTick(() => {
  //     editedProduct.value = Object.assign({}, initilProduct)
  //   })
  // }

  // function deleteItemConfirm() {
  //   //delete item from list
  //   products.value.splice(editedIndex, 1)
  //   closeDelete()
  // }

  // function editItem(item: Product) {
  //   editedIndex = products.value.indexOf(item)
  //   editedProduct.value = Object.assign({}, item)
  //   dialog.value = true
  // }

  // function deleteItem(item: Product) {
  //   editedIndex = products.value.indexOf(item)
  //   editedProduct.value = Object.assign({}, item)
  //   dialogDelete.value = true
  //   editedIndex = -1
  // }

  // async function save() {
  //   const { valid } = await refForm.value!.validate()

  //   if (!valid) return

  //   if (editedIndex > -1) {
  //     Object.assign(products.value[editedIndex], editedProduct.value)
  //   } else {
  //     products.value.push(editedProduct.value)
  //     editedProduct.value.id = lastId++
  //   }
  //   editedIndex = -1
  //   close()
  // }

  // function close() {
  //   dialog.value = false
  //   nextTick(() => {
  //     editedProduct.value = Object.assign({}, initilProduct)
  //     editedIndex = -1
  //   })
  // }
  return {

    Product2,
    Product3,
    Product4,
    defaultProducts,
    LatestId,
    LastID,
    getProduct,
    saveProduct,
    deleteUser,
    getProducts,
    products
  }
})
