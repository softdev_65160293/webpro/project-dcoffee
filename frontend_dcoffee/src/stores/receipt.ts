import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMembersStore } from './member'

export const useReceipetStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMembersStore()
  const receiptItems = ref<ReceiptItem[]>([])
  const receiptDialog = ref(false)
  let pointValue = 0
  let currentReceiptTotal = 0
  let currentPoint = 0
  const receipt = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    totalBefore: 0,
    total: 0,
    receicedAmount: 0,
    change: 0,
    memberDiscount: 0,
    paymentType: 'cash',
    userId: authStore.currentUser!.id,
    user: authStore.currentUser!,
    memberID: 0
  })

  const receiptItemTest = ref<ReceiptItem[]>([
    {
      id: 1,
      name: 'ลาเต้เย็น',
      price: 300,
      unit: 1,
      productId: 1,
      type: 1
    },
    {
      id: 2,
      name: 'ลาเต้ร้อน',
      price: 200,
      unit: 1,
      productId: 1,
      type: 1
    },
    {
      id: 3,
      name: 'ลาเต้ปั่น',
      price: 250,
      unit: 1,
      productId: 1,
      type: 1
    }
  ])

  const receipts = ref<Receipt[]>([
    {
      id: 1,
      createdDate: new Date(2024, 1, 1, 9, 45, 40),
      totalBefore: 100,
      total: 90,
      receicedAmount: 500,
      change: 410,
      memberDiscount: 10,
      paymentType: 'cash',
      userId: authStore.users[0].id,
      user: authStore.users[0],
      memberID: 1
    },
    {
      id: 2,
      createdDate: new Date(2024, 1, 2, 11, 20, 30),
      totalBefore: 1000,
      total: 900,
      receicedAmount: 1000,
      change: 100,
      memberDiscount: 100,
      paymentType: 'scan',
      userId: authStore.users[1].id,
      user: authStore.users[1],
      memberID: 2
    },
    {
      id: 3,
      createdDate: new Date(2024, 1, 3, 10, 33, 30),
      totalBefore: 500,
      total: 450,
      receicedAmount: 600,
      change: 150,
      memberDiscount: 50,
      paymentType: 'cash',
      userId: authStore.users[2].id,
      user: authStore.users[2],
      memberID: 3
    }
  ])

  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceipt: ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id,
        type: product.type,
        product: product
      }
      receiptItems.value.push(newReceipt)
      calReceipt()
    }
  }
  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function Inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }

  function Dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }

  function calChange() {
    if (receipt.value.receicedAmount > 0) {
      receipt.value.change = receipt.value.receicedAmount - receipt.value.total
    } else {
      receipt.value.change = 0
    }
  }

  function calReceipt() {
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + item.price * item.unit
    }
    receipt.value.totalBefore = totalBefore
    if (memberStore.currentMember) {
      receipt.value.total = totalBefore * 0.95
      receipt.value.memberDiscount = totalBefore - receipt.value.total
      receipt.value.recieptItem = receiptItems.value
    } else {
      receipt.value.total = totalBefore
      receipt.value.recieptItem = receiptItems.value
    }
  }

  function calUsePoint(point: string) {
    if (memberStore.currentMember) {
      pointValue = parseInt(point)
      let totalBefore = 0
      for (const item of receiptItems.value) {
        totalBefore = totalBefore + item.price * item.unit
      }
      currentReceiptTotal = totalBefore
      if (totalBefore < pointValue / 10) {
        receipt.value.memberDiscount = receipt.value.totalBefore
        receipt.value.total = receipt.value.totalBefore - receipt.value.memberDiscount
      } else {
        receipt.value.total = totalBefore * 0.95
        receipt.value.memberDiscount = totalBefore - receipt.value.total + pointValue / 10
        receipt.value.total = receipt.value.total - receipt.value.memberDiscount
      }
    } else {
      return
    }
  }

  function removePoint() {
    if (memberStore.currentMember) {
      memberStore.currentMember.point = memberStore.currentMember.point - pointValue
    }
  }

  function addPoint() {
    if (memberStore.currentMember) {
      currentPoint = memberStore.currentMember.point + Math.round(currentReceiptTotal / 10)
      memberStore.currentMember.point = currentPoint
    }
  }

  function showReceiptDialog() {
    calChange()
    receipt.value.recieptItem = receiptItems.value
    receiptDialog.value = true
  }

  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      total: 0,
      receicedAmount: 0,
      change: 0,
      memberDiscount: 0,
      paymentType: 'cash',
      userId: authStore.currentUser!.id,
      user: authStore.currentUser!,
      memberID: 0
    }
    currentPoint = 0
    pointValue = 0
    currentReceiptTotal = 0
    memberStore.clear()
  }

  return {
    addReceiptItem,
    removeReceiptItem,
    Inc,
    Dec,
    calReceipt,
    showReceiptDialog,
    clear,
    calUsePoint,
    addPoint,
    calChange,
    removePoint,
    receiptItems,
    receipt,
    receiptDialog,
    receipts,
    currentPoint,
    pointValue
  }
})
