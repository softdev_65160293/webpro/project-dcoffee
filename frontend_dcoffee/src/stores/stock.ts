import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Material } from '@/types/Material'

export const useStockStore = defineStore('stock', () => {
  const headers = [
    {
      title: 'Id',
      key: 'id'
    },
    {
      title: 'Name',
      key: 'name'
    },
    {
      title: 'Price',
      key: 'price'
    },
    {
      title: 'Unit',
      key: 'unit'
    },
    {
      title: 'Minimum',
      key: 'min'
    },
    {
      title: 'Types',
      key: 'types',
      value: (item: any | Material) => {
        if (Array.isArray(item.types) && item.types.length > 0) {
          return item.types.join(', ')
        } else if (item.types !== null && item.types !== undefined) {
          return item.types.toString()
        }
      }
    },
    { title: 'Actions', key: 'actions', sortable: false }
  ]

  const initilMaterial: Material = {
    id: -1,
    name: '',
    price: 0,
    unit: 0,
    min: 0,
    types: ['pack']
  }

  let editedIndex = -1
  const editedMaterial = ref<Material>(JSON.parse(JSON.stringify(initilMaterial)))
  const materials = ref<Material[]>([])

  function initialize() {
    materials.value = [
      {
        id: 1,
        name: 'Milk',
        price: 50,
        unit: 20,
        min: 10,
        types: ['bottle']
      },
      {
        id: 2,
        name: 'Tea Leaves',
        price: 120,
        unit: 8,
        min: 10,
        types: ['bottle']
      },
      {
        id: 3,
        name: 'Green Tea Leaves',
        price: 150,
        unit: 10,
        min: 10,
        types: ['bottle']
      },
      {
        id: 4,
        name: 'Cups',
        price: 80,
        unit: 15,
        min: 20,
        types: ['pack']
      },
      {
        id: 5,
        name: 'Sugar',
        price: 65,
        unit: 10,
        min: 10,
        types: ['pack']
      },
      {
        id: 6,
        name: 'Lime',
        price: 50,
        unit: 5,
        min: 5,
        types: ['pack']
      },
      {
        id: 7,
        name: 'Honey',
        price: 75,
        unit: 10,
        min: 5,
        types: ['bottle']
      },
      {
        id: 8,
        name: 'Straw',
        price: 50,
        unit: 20,
        min: 20,
        types: ['pack']
      },
      {
        id: 9,
        name: 'Chocolate Chips',
        price: 100,
        unit: 5,
        min: 5,
        types: ['pack']
      },
      {
        id: 10,
        name: 'Cream',
        price: 120,
        unit: 5,
        min: 5,
        types: ['pack']
      }
    ]
  }

  const CheckDialog = ref(false)
  const form = ref(false)
  const loading = ref(false)

  function CheckStock(item: Material) {
    editedIndex = materials.value.indexOf(item)
    editedMaterial.value = Object.assign({}, item)
    CheckDialog.value = true
  }

  const onSubmit = function () {}

  function closeDialog() {
    CheckDialog.value = false
    nextTick(() => {
      editedMaterial.value = Object.assign({}, initilMaterial)
    })
  }

  return {
    CheckStock,
    onSubmit,
    closeDialog,
    initialize,
    headers,
    CheckDialog,
    form,
    loading,
    editedMaterial,
    materials
  }
})
