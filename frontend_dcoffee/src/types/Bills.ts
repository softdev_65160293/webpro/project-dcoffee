type Bills = {
  id: number
  type: string
  usedUnit: number
  unitToBath: number
  totalPrice: number
  date: Date
}

export { type Bills }
