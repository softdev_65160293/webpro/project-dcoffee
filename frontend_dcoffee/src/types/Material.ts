type Type = 'pack' | 'bottle' | 'others'

type Material = {
  id: number
  name: string
  price: number
  unit: number
  min: number
  types: Type[]
}

export type { Material }
