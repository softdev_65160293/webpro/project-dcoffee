import type { User } from './User'
import type { Material } from './Material'

type OrderReceipt = {
  id: number
  createdDate: Date
  totalQty: number
  totalPrice: number
  userId: number
  user?: User
  Material?: Material
}

export type { OrderReceipt }
