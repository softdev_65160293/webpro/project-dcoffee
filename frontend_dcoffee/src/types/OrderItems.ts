import type { Material } from './Material'

type OrderItems = {
  id: number
  name: string
  price: number
  qty: number
  materialId: number
  material?: Material
}

export { type OrderItems }
