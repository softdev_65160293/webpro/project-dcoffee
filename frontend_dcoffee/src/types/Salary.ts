import type { History } from './History'
import type { User } from './User'

type Salary = {
  id: number
  name: string
  email: string
  salary: string
  status: boolean
  date: Date
  userId?: number
  user?: User
  historyCheckin?: History[]
  historyCheckout?: History[]
}

export type { Salary }
