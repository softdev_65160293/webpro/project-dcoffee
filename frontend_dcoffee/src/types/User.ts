type Gender = 'male' | 'female' | 'others'
type Role = 'admin' | 'user'

type User = {
  id: number
  email: string
  username: string
  password: string
  fullname: string
  gender: Gender // Male, Female, Others
  roles: Role[] // admin, user
}

export type { Gender, Role, User }
